import { MenuItem } from '@/types/menuItem';

export const menu: Array<MenuItem> = [
  {
    title: 'Projects',
    icon: 'mdi-folder',
    routeName: 'projectListing'
  },
  {
    title: 'Invoices',
    icon: 'mdi-receipt',
    routeName: 'invoiceListing'
  },
  {
    title: 'Customers',
    icon: 'mdi-account-group-outline',
    routeName: 'customerListing'
  }
];
