import store from '@/store';
import { boot } from 'quasar/wrappers';

export default boot(({ app }) => {
  app.use(store);
});
