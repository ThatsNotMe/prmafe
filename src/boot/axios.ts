import { boot } from 'quasar/wrappers';
import axios, { AxiosRequestConfig } from 'axios';
import store from '@/store';

const config: AxiosRequestConfig = {};
config.baseURL = 'http://localhost:8080';
config.headers = {};
const api = axios.create(config);

api.interceptors.request.use(function (config: AxiosRequestConfig): AxiosRequestConfig {
  const token: null | string = store.getters['user/accessToken'];
  if (token !== null && config.headers) {
    config.headers.Authorization = token;
  }
  return config;
});

export default boot(({ app }) => {
  app.config.globalProperties.$axios = axios;
  app.config.globalProperties.$api = api;
});

export { axios, api };
