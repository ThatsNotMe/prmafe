import { RouteRecordRaw } from 'vue-router';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'home',
    component: () => import(/* webpackChunkName: "home" */ '../pages/Home.vue')
  },
  {
    path: '/projects',
    name: 'projectListing',
    component: () =>
      import(
        /* webpackChunkName: "projectListing" */ '../pages/ProjectListing.vue'
      ),
    meta: { requiresAuth: true }
  },
  {
    path: '/invoices',
    name: 'invoiceListing',
    component: () =>
      import(
        /* webpackChunkName: "invoiceListing" */ '../pages/InvoiceListing.vue'
      ),
    meta: { requiresAuth: true }
  },
  {
    path: '/customers',
    name: 'customerListing',
    component: () =>
      import(
        /* webpackChunkName: "customerListing" */ '../pages/CustomerListing.vue'
      ),
    meta: { requiresAuth: true }
  },

  // Always leave this as last one,
  {
    path: '/:errorUrl(.*)',
    name: 'error404',
    component: () =>
      import(/* webpackChunkName: "error404" */ '../pages/Error404.vue')
  }
];

export default routes;
