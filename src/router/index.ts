import store, { RootState } from '@/store';
import { Notify } from 'quasar';
import { route } from 'quasar/wrappers';
import {
  createMemoryHistory,
  createRouter,
  createWebHashHistory,
  createWebHistory
} from 'vue-router';
import routes from './routes';

export default route<RootState>(function (/* { store, ssrContext } */) {
  const createHistory = process.env.SERVER
    ? createMemoryHistory
    : process.env.VUE_ROUTER_MODE === 'history'
      ? createWebHistory
      : createWebHashHistory;

  const Router = createRouter({
    scrollBehavior: () => ({ left: 0, top: 0 }),
    routes,

    // Leave this as is and make changes in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    history: createHistory(
      process.env.MODE === 'ssr' ? void 0 : process.env.VUE_ROUTER_BASE
    )
  });

  Router.beforeEach((to, from, next) => {
    store.commit('main/setPageNotFound', false);
    if (to.matched.some(record => record.meta.requiresAuth)) {
      if (store.getters['user/isLoggedIn']) {
        next();
        return;
      }
      Router.push({ name: 'home' });
      Notify.create({
        type: 'negative',
        message: 'Not logged in.',
        timeout: 500
      });
    } else {
      next();
    }
  });

  return Router;
});
