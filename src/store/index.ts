import { createStore } from 'vuex';
import main from './modules/main';
import user from './modules/user';
import project from './modules/project';
import invoice from './modules/invoice';
import customer from './modules/customer';

export type RootState = {
  user: typeof user.state;
  invoice: typeof invoice.state;
  project: typeof project.state;
  customer: typeof customer.state;
}

export default createStore({
  modules: {
    main,
    user,
    project,
    invoice,
    customer
  }
  // strict: process.env.DEBUGGING
});
