import { api } from '@/boot/axios';
import { ActionContext } from 'vuex';
import { RootState } from '@/store';
import { CustomerType } from '@/types/customerType';
import { Notify } from 'quasar';

export type CustomerState = {
  customers: Array<CustomerType>;
  customersOptions: Array<any>;
};

const state: CustomerState = {
  customers: [],
  customersOptions: []
};

const mutations = {
  setCustomers (state: CustomerState, customers: Array<CustomerType>) {
    state.customers = customers;
  },
  setCustomersOptions (state: CustomerState, customersOptions: Array<any>) {
    state.customersOptions = customersOptions;
  }
};

const actions = {
  async fetchCustomers ({ commit }: ActionContext<CustomerState, RootState>) {
    const response = await api.get('api/customers');

    const options: any[] = [];
    const result: any[] = [];
    response.data.map(({ name, surname, companyName, id, address, ico }: any) => {
      options.push({
        value: id,
        label: companyName || `${name} ${surname}`
      });
      result.push({
        id,
        companyName,
        name,
        surname,
        ico,
        address
      });
    });

    commit('setCustomersOptions', options);
    commit('setCustomers', result);
  },

  async addCustomer (
    { commit }: ActionContext<CustomerState, RootState>,
    payload: any
  ) {
    await api.post('api/customers', payload);
  },

  async editCustomer (
    { commit }: ActionContext<CustomerState, RootState>,
    payload: any
  ) {
    await api.put('api/customers', payload);
  },

  async deleteCustomer (
    { commit }: ActionContext<CustomerState, RootState>,
    id: string
  ) {
    try {
      await api.delete('api/customers/' + id);
    } catch (error: any) {
      if (error.response.data.status === 409) {
        const message = error.response.data.message as string;
        Notify.create({ type: 'negative', message });
      }
    }
  }
};

const getters = {
  getCustomerById: (state: CustomerState) => async (id: number) => {
    const response = await api.get('api/customers/' + id);
    return response.data;
  }
};

const InvoiceModule = {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};

export default InvoiceModule;
