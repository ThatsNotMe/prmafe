import { api } from '@/boot/axios';
import { ActionContext } from 'vuex';
import { RootState } from '@/store';

export type ProjectState = {
  projects: Array<any>;
};

const state: ProjectState = {
  projects: []
};

const mutations = {
  setProjects (state: ProjectState, projects: Array<any>) {
    state.projects = projects;
  }
};

const actions = {
  async fetchProjects ({ commit }: ActionContext<ProjectState, RootState>) {
    const response = await api.get('api/projects');

    const result: any[] = [];
    response.data.map(
      ({
        id,
        desc,
        estimatedHours,
        estimatedPrice,
        finalPrice,
        hourRate,
        hours,
        numberOfDev,
        status,
        title
      }: any) => {
        result.push({
          id,
          title,
          desc,
          estimatedHours,
          estimatedPrice: estimatedHours * hourRate,
          finalPrice: hourRate * hourRate,
          hourRate,
          hours,
          numberOfDev,
          status
        });
      }
    );
    commit('setProjects', result);
  },

  async addProject (
    { commit }: ActionContext<ProjectState, RootState>,
    payload: any
  ) {
    await api.post('api/projects', payload);
  },

  async editProject (
    { commit }: ActionContext<ProjectState, RootState>,
    payload: any
  ) {
    await api.put('api/projects', payload);
  },

  async deleteProject (
    { commit }: ActionContext<ProjectState, RootState>,
    id: string
  ) {
    await api.delete('api/projects/' + id);
  }
};

const InvoiceModule = {
  namespaced: true,
  state,
  mutations,
  actions
};

export default InvoiceModule;
