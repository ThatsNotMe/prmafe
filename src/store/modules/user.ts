import { api } from '@/boot/axios';
import { ActionContext } from 'vuex';
import { RootState } from '@/store';
import jwtDecode from 'jwt-decode';

export type UserStateProfile = {
  [index: string]: any;
};

export type UserState = {
  accessToken: null | string;
  isLoggedIn: boolean;
  profile: any;
};

const state = (): UserState => ({
  accessToken: null,
  isLoggedIn: false,
  profile: null
});

const mutations = {
  setSession (state: UserState, data: any) {
    state.accessToken = data.accessToken;
    state.isLoggedIn = true;
    state.profile = data.profile;
  }
};

const actions = {
  async applyAccessToken (
    context: ActionContext<UserState, RootState>,
    accessToken: string
  ) {
    return new Promise<void>(function (resolve, reject) {
      try {
        const data = {
          profile: null,
          accessToken: accessToken,
          isLoggedIn: true
        };
        context.commit('setSession', data);
        localStorage.setItem('session', JSON.stringify(data));
        context.dispatch('whoIsMe');
        resolve();
      } catch (err) {
        localStorage.clear();
        reject(err);
      }
    });
  },

  async signUp ({ commit }: ActionContext<UserState, RootState>, payload: any) {
    await api.post('api/users/signup', payload);
  },
  async saveUser ({ commit }: ActionContext<UserState, RootState>, payload: any) {
    await api.post('api/users/edit', payload);
  },
  async login (context: ActionContext<UserState, RootState>, payload: any) {
    const response = await api.post('login', payload);
    context.dispatch('applyAccessToken', response.headers.authorization);
  },
  async logout ({ commit }: ActionContext<UserState, RootState>, payload: any) {
    localStorage.clear();
    const data = {
      profile: null,
      accessToken: null,
      isLoggedIn: false
    };
    commit('setSession', data);
  },

  getSession: (context: ActionContext<UserState, RootState>) => {
    api.interceptors.response.use(undefined, (error: any) => {
      return new Promise<void>(function (resolve, reject) {
        if (error && error.response.status === 403) {
          context.dispatch('logout');
        }
        throw error;
      });
    });
    const session = localStorage.getItem('session');
    if (session && typeof session === 'string' && session !== '') {
      const data = JSON.parse(session);
      context.commit('setSession', data);
    }
  },

  async whoIsMe ({ commit }: ActionContext<UserState, RootState>) {
    const response = await api.get('api/users/me');
    const session = localStorage.getItem('session');
    if (session && typeof session === 'string' && session !== '') {
      const data = JSON.parse(session);
      data.profile = response.data;
      localStorage.setItem('session', JSON.stringify(data));
      commit('setSession', data);
    }
  }
};

const getters = {
  accessToken: (state: UserState) => state.accessToken,
  profile: (state: UserState) => state.profile,
  isLoggedIn: (state: UserState) => state.isLoggedIn
};

const UserModule = {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};

export default UserModule;
