export type MainState = {
    pageNotFound: boolean;
  };

const state = (): MainState => ({
  pageNotFound: false
});

const mutations = {
  setPageNotFound (state: MainState, payload: boolean): void {
    state.pageNotFound = payload;
  }
};

const actions = {};

const MainModule = {
  namespaced: true,
  state,
  mutations,
  actions
};

export default MainModule;
