import { api } from '@/boot/axios';
import { ActionContext } from 'vuex';
import store, { RootState } from '@/store';
import { date } from 'quasar';

export type InvoiceState = {
  invoices: Array<any>;
};

const paymentMethods = [
  { label: 'Platba převodem', value: 'bank' },
  { label: 'Proforma', value: 'proforma' },
  { label: 'Platba hotovostí', value: 'cash' }
];

const state: InvoiceState = {
  invoices: []
};

const mutations = {
  setInvoices (state: InvoiceState, invoices: Array<any>) {
    state.invoices = invoices;
  }
};

const actions = {
  async fetchInvoices ({ commit }: ActionContext<InvoiceState, RootState>) {
    const response = await api.get('api/invoice');

    const result: any[] = [];

    await Promise.all(
      response.data.map(
        async ({
          customerId,
          id,
          invoiceItems,
          invoiceNumber,
          issueDate,
          paymentMethod,
          priceTotal,
          status
        }: any) => {
          const customer = await store.getters['customer/getCustomerById'](
            customerId
          );
          result.push({
            id,
            customer: {
              label:
                customer.companyName || `${customer.name} ${customer.surname}`,
              value: customer.id
            },
            invoiceItems,
            invoiceNumber,
            invoiceDate: date.formatDate(issueDate, 'DD.MM.YYYY'),
            paymentMethod: paymentMethods.find(
              (item: any) => item.value === paymentMethod
            ),
            status,
            price: priceTotal
          });
        }
      )
    );
    commit('setInvoices', result);
  },

  async deleteInvoice (
    { commit }: ActionContext<InvoiceState, RootState>,
    id: string
  ) {
    await api.delete('api/invoice/' + id);
  },

  async addInvoice (
    { commit }: ActionContext<InvoiceState, RootState>,
    payload: any
  ) {
    await api.post('api/invoice', payload);
  },

  async editInvoice (
    { commit }: ActionContext<InvoiceState, RootState>,
    payload: any
  ) {
    await api.put('api/invoice', payload);
  }
};

const getters = {
  getInvoiceById: (state: InvoiceState) => async (id: number) => {
    const response = await api.get('api/invoice/' + id);
    let result = {};
    await Promise.all(
      response.data.map(
        async ({
          customerId,
          id,
          invoiceItems,
          invoiceNumber,
          issueDate,
          paymentMethod,
          priceTotal
        }: any) => {
          const customer = await store.getters['customer/getCustomerById'](
            customerId
          );
          result = {
            id,
            customer,
            invoiceItems,
            invoiceNumber,
            invoiceDate: date.formatDate(issueDate, 'DD.MM.YYYY'),
            dueDate: date.formatDate(date.addToDate(issueDate, { days: 14 }), 'DD.MM.YYYY'),
            paymentMethod: paymentMethods.find(
              (item: any) => item.value === paymentMethod
            )?.value,
            price: priceTotal
          };
        }
      )
    );
    return result;
  }
};

const InvoiceModule = {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};

export default InvoiceModule;
