import { CustomerType } from './customerType';
import { InvoiceItemType } from './invoiceItemType';

export interface InvoiceExportType {
  customer: CustomerType;
  invoiceItems: InvoiceItemType[];
  invoiceDate: string;
  dueDate: string;
  paymentMethod: string;
  price: number;
  invoiceNumber: number;
}
