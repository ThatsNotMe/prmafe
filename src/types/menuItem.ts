export interface MenuItem {
  title: string,
  icon: string,
  routeName?: string,
  children?: MenuItem[]
}
