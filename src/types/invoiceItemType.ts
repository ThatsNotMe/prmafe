export interface InvoiceItemType {
  unitsOfMeasure: string,
  description: string,
  numberOfUnits: number,
  pricePerUnit: number,
  price: number
}
