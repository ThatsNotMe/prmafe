export interface CustomerType {
  id?: string;
  companyName?: string;
  name?: string;
  surname?: string;
  ico: string;
  dic: string;
  address: {
    street: string;
    city: string;
    zip: string;
    houseNumber: string;
  };
}
